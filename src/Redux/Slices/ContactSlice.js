import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios';

export const getContact = createAsyncThunk('contact/getContact', async () => {
  let data = {};
  await axios
    .get('https://contact.herokuapp.com/contact')
    .then((response) => {
      data = response.data;
      console.log('dev getContact', response.data)
    })
  return data;
});

export const addContact = createAsyncThunk('contact/addContact', async (param) => {
  let data = {};
  await axios
    .post('https://contact.herokuapp.com/contact', param)
    .then((response) => {
      data = response;
      if (response.status === 201) console.log('sukses');
      console.log('dev addContact', response, param, response.status === 201);
    })
    .catch((error) => {
      console.log('dev err', error);
    })
  return data;
});

export const uploadImage = createAsyncThunk('contact/uploadImage', async (param) => {
  let data = {};
  console.log('dev uploadImage param', param);
  await axios
    .post('https://freeimage.host/api/1/upload', {
      key: '6d207e02198a847aa98d0a2a901485a5',
      source: param,
    })
    .then((response) => {
      data = response;
      // if (response.status === 201) console.log('sukses');
      console.log('dev uploadImage', response, param);
    })
    .catch((error) => {
      console.log('dev err', error);
    })
  return data;
});

export const editContact = createAsyncThunk('contact/editContact', async (param) => {
  let data = {};
  await axios
    .put(`https://contact.herokuapp.com/contact/${param.id}`, param.data)
    .then((response) => {
      data = response;
      if (response.status === 201) console.log('sukses');
      console.log('dev editContact', response, param, response.status === 201);
    })
    .catch((error) => {
      console.log('dev err', error);
    })
  return data;
});

export const deleteContact = createAsyncThunk('contact/deleteContact', async (param) => {
  let data = {};
  await axios
    .delete(`https://contact.herokuapp.com/contact/${param}`, { method: 'DELETE' })
    .then((response) => {
      data = response;
      // if (response.status === 201) console.log('sukses');
      console.log('dev deleteContact', response, param);
    })
    .catch((error) => {
      console.log('dev err', error);
    })
  return data;
});

const initialState = {
  data: [],
  selectedContact: {},
  openModal: false,
  inputTitle: '',
  inputFirstName: '',
  inputLastName: '',
  inputAge: '',
  inputPhoto: 'N/A',
  isLoading: false,
  isError: null,
}

export const contactSlice = createSlice({
  name: 'contact',
  initialState,
  reducers: {
    setSelectContact: (state, action) => {
      state.selectedContact = action.payload;
    },
    setOpen: (state, action) => {
      state.openModal = action.payload;
    },
    setTitle: (state, action) => {
      state.inputTitle = action.payload;
    },
    setFirstName: (state, action) => {
      state.inputFirstName = action.payload;
    },
    setLastName: (state, action) => {
      state.inputLastName = action.payload;
    },
    setAge: (state, action) => {
      state.inputAge = action.payload;
    },
    setPhoto: (state, action) => {
      state.inputPhoto = action.payload;
    },
    resetInput: (state, action) => {
      state.inputFirstName = '';
      state.inputLastName = '';
      state.inputAge = '';
      state.inputPhoto = '';
    }
  }, 
  extraReducers: {
    [getContact.pending]: (state) => {
      state.isLoading = true;
    },
    [getContact.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.data = action.payload.data;
    },
    [getContact.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
    [addContact.pending]: (state) => {
      state.isLoading = true;
    },
    [addContact.fulfilled]: (state, action) => {
      state.isLoading = false;
      // state.data = action.payload.data;
    },
    [addContact.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
    [deleteContact.pending]: (state) => {
      state.isLoading = true;
    },
    [deleteContact.fulfilled]: (state, action) => {
      state.isLoading = false;
      // state.data = action.payload.data;
    },
    [deleteContact.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
  },
})

// Action creators are generated for each case reducer function
export const { setSelectContact, setOpen, setTitle, setFirstName, setLastName, setAge, setPhoto, resetInput } = contactSlice.actions

export default contactSlice.reducer