import { configureStore } from '@reduxjs/toolkit'
import contactReducer from '../Redux/Slices/ContactSlice'

export const store = configureStore({
  reducer: {
    contact: contactReducer
  },
})