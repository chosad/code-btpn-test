import { Avatar } from '@mui/material'
import React from 'react'
import { useDispatch } from 'react-redux';
import { setSelectContact } from '../../Redux/Slices/ContactSlice';

const CardListContact = ({data}) => {
  const dispatch = useDispatch();
  const {firstName, lastName, photo, id} = data;
  return (
    <div className='flex gap-4 items-center shadow-md p-3 my-3 cursor-pointer' onClick={() => dispatch(setSelectContact(data))}>
      <Avatar src={photo} />
      <div>
        <p className='text-[18px]'>{firstName}</p>
        <p className='text-[12px]'>{lastName}</p>
      </div>
      <p>{id}</p>
    </div>
  )
}

export default CardListContact