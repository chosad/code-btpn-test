import { Avatar, Button, Divider } from '@mui/material'
import React from 'react'
import FacebookIcon from '@mui/icons-material/Facebook';
import XIcon from '@mui/icons-material/X';
import InstagramIcon from '@mui/icons-material/Instagram';
import { useDispatch, useSelector } from 'react-redux';
import { deleteContact, setAge, setFirstName, setLastName, setOpen, setPhoto, setTitle } from '../../Redux/Slices/ContactSlice';

const DetailContact = () => {
  const data = useSelector(state => state.contact.selectedContact)
  const dispatch = useDispatch();
  const arrPekerjaan = ['Developer', 'Grafik Desainer', 'Quality Assurance', 'Frontend Dev', 'Backend Dev'];
  const arrPhone = ['08123456789', '08135662648', '08561237654', '0878239862', '08987654312'];
  const arrAddress = [
    '123 Main Street, Springfield, IL 62701',
    '456 Oak Avenue, New York, NY 10001',
    '789 Elm Street, Los Angeles, CA 90001',
    '101 Pine Road, Chicago, IL 60601',
    '202 Maple, San Francisco, CA 94101'
  ];

  const handleEdit = () => {
    dispatch(setOpen(true))
    dispatch(setTitle('Edit contact'))
    dispatch(setFirstName(data.firstName))
    dispatch(setLastName(data.lastName))
    dispatch(setAge(data.age))
    dispatch(setPhoto(data.photo))
  }

  const randomValue = (arr) => arr[Math.floor(Math.random() * arr.length)];
  const detailData = [['Phone Number', randomValue(arrPhone)], ['Email', `${data.firstName}.${data.lastName}@example.com`], ['Address', randomValue(arrAddress)]]
  return (
    Object.keys(data).length ? 
    <div>
      <div className='flex items-center gap-5 p-3'>
        <Avatar sx={{ width: 70, height: 70 }} src={data.photo}/>
        <div>
          <div>{data.firstName + ' ' + data.lastName}</div>
          <div>{randomValue(arrPekerjaan)}</div>
          <div className='flex gap-3 mt-3'>
            <FacebookIcon />
            <XIcon />
            <InstagramIcon />
            
          </div>
        </div>
      </div>
      <div className='p-3'>
        <p className='my-3'>Personal detail</p>
        <Divider />
        {detailData.map(x => 
          <div className='flex my-2'>
            <p className='w-2/4'>{x[0]}</p>
            <p>{x[1]}</p>
          </div>
        )}
        <div>
          <Button onClick={handleEdit}>Edit</Button>
          <Button onClick={() => dispatch(deleteContact(data.id))}>Delete</Button>
        </div>
        <div>
          <p className='text-[13px] mt-7'>Note: Data personal random with dummy data</p>
          <p className='text-[13px]'>Note: Delete server response error</p>
        </div>
      </div>
    </div>
    : 
    <div className='flex flex-col justify-center items-center'>
      <p>Detail Contact</p>
    </div>
  )
}

export default DetailContact