import React, { useEffect } from 'react'
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import CardListContact from '../../Components/CardListContact/CardListContact';
import DetailContact from '../../Components/DetailContact/DetailContact';
import { useDispatch, useSelector } from 'react-redux';
import { addContact, editContact, getContact, resetInput, setAge, setFirstName, setLastName, setOpen, setPhoto, setSelectContact, setTitle } from '../../Redux/Slices/ContactSlice';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Skeleton, TextField } from '@mui/material';
import iconicImage from '../../Images/iconic.jpeg'

const Home = () => {
  const data = useSelector(state => state.contact.data);
  const selectedContact = useSelector(state => state.contact.selectedContact);
  const open = useSelector(state => state.contact.openModal);
  const title = useSelector(state => state.contact.inputTitle);
  const first = useSelector(state => state.contact.inputFirstName);
  const last = useSelector(state => state.contact.inputLastName);
  const age = useSelector(state => state.contact.inputAge);
  const photo = useSelector(state => state.contact.inputPhoto);
  const loading = useSelector(state => state.contact.isLoading);
  const dispatch = useDispatch();
  console.log('dev redux', {first, last, age})

  useEffect(() => {
    dispatch(getContact())
  }, [dispatch]);

  const handleClose = () => {
    dispatch(setOpen(false));
    dispatch(resetInput())
  }

  const handleAdd = () => {
    const param = {
      "firstName": first,
      "lastName": last,
      "age": age,
      "photo": photo || 'N/A',
    }
    console.log('dev photo', photo)
    if (title === 'Add new contact') {
        dispatch(addContact(param)).then(Response => {
          if (Response.payload.status === 201) {
            handleClose()
            dispatch(getContact())
          }
        })
    } else {
      dispatch(editContact({id: selectedContact.id, data: param})).then(Response => {
        if (Response.payload.status === 201) {
          dispatch(setSelectContact({...param, id: selectedContact.id}))
          handleClose()
          dispatch(getContact())
        }
      })
    }
  }

  const getBase64 = (file) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result);
      dispatch(setPhoto(reader.result))
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
    return reader.result
 }

  const handleImageChange = (event) => {
    const file = event.target.files[0];
    const base64Img = getBase64(file);
    console.log('dev base64', base64Img)
    // const imageUrl = URL.createObjectURL(file);
    // console.log('dev image', imageUrl, event.target.files[0]);
    // dispatch(setPhoto(base64Img))
  };

  console.log('loading', loading)
  return (
    <div className='h-screen'>
      <div className='bg-[#8B93FF] p-7 flex justify-between items-center '>
        <div className='text-white flex items-center gap-5'>
          <img src={iconicImage} alt='asd' style={{width: 70}}/>
          Choirul Contact List
        </div>
        <div>
          <Avatar children='CS' />
        </div>
      </div>
      <div className='flex h-full'>
        <div className='bg-[#5755FE] p-6 h-full text-slate-900'>
          <div className='bg-[#EDEEF0] p-4 rounded-lg' onClick={() => { dispatch(setTitle('Add new contact')); dispatch(setOpen(true));}}>New Contact</div>
        </div>
        <div className='bg-[#EDEEF0] p-5 flex-1 flex'>
          <div className='w-2/4 pr-3 overflow-auto'>
            {loading ? 
              <div>
                <Skeleton variant="rectangular" width='100%' height={100} />
                <Skeleton variant="rectangular" width='100%' height={50} className='my-2'/>
                <Skeleton variant="rectangular" width='100%' height={40} />
              </div>
            : data.map(x => 
              <CardListContact data={x}/>
            )}
          </div>
          <Divider orientation="vertical" variant="middle" flexItem />
          <div className='p-3 flex-1'>
            <DetailContact/>
          </div>
        </div>
      </div>
      <Dialog
        maxWidth={'sm'}
        fullWidth
        open={open}
        className='rounded-3xl'
      >
        <DialogTitle className='flex justify-center'>{title}</DialogTitle>
        <DialogContent className='flex flex-col gap-5'>
          <div className='flex items-center gap-4 justify-center'>
            <Avatar src={photo} sx={{ width: 100, height: 100 }}/>
            <input type="file" accept="image/*" onChange={handleImageChange} />
          </div>
          <TextField label="First Name" variant="outlined" onChange={(e) => dispatch(setFirstName(e.target.value))} value={first}/>
          <TextField label="Last Name" variant="outlined" onChange={(e) => dispatch(setLastName(e.target.value))} value={last}/>
          <TextField label="Age" variant="outlined" onChange={(e) => dispatch(setAge(e.target.value))} value={age}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleAdd} disabled={first === '' || last === '' || age === ''}>Save</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default Home